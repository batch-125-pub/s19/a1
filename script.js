function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;
	this.tackle = function(target){
		target.health = target.health - this.attack;
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to ${target.health}`);
		if(target.health < 10){
			console.log(target.faint());
		}
	}
	this.faint = function(){
		return `${this.name} fainted`;
	}
}


let pikachu = new Pokemon("Pikachu", 5, 50);

let charizard = new Pokemon("Charizard", 8, 40);

let squirtle = new Pokemon("Squirtle", 4, 80);

let bulbasaur = new Pokemon("Bulbasaur", 4, 30);

let snorlax = new Pokemon("Snorlax", 10, 20);

for(let i = 0; i < 6; i++){
	snorlax.tackle(bulbasaur);
}

